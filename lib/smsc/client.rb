# frozen_string_literal: true

require "logger"

require "faraday"

require_relative "client/validation/balance_response_contract"
require_relative "client/validation/error_response_contract"
require_relative "client/validation/send_message_response_contract"
require_relative "client/validation/call_to_verify_response_contract"
require_relative "client/error"
require_relative "client/version"

##
# Main module
#
module SMSC
  ##
  # SMSC.ru client
  #
  class Client
    # Документация по параметрам: https://smsc.ru/api/http/send/sms
    DEFAULT_PARAMS = {
      # Ответ в json формате
      fmt: 3
    }.freeze

    ##
    # @param login [String] SMSC login
    # @param password [String] SMSC password
    # @param sender [String] SMSC registered sender name
    # @param logging [Boolean] true by default
    # @param logger [Logger] custom logger
    # @param adapter [Faraday::Adapter] custom Faraday HTTP adapter
    #
    def initialize(**params)
      @login    = params.fetch(:login)
      @password = params.fetch(:password)
      @sender   = params.fetch(:sender)

      @faraday = Faraday.new do |faraday|
        faraday.request :json

        # Response timeout
        faraday.options[:timeout] = 10
        faraday.options[:open_timeout] = 10

        # Logging
        logging = params.fetch(:logging) { true }

        logger  = params.fetch(:logger) do
          if defined?(Rails)
            Rails.logger
          else
            Logger.new($stdout)
          end
        end

        if logging && logger
          faraday.response :logger, logger, bodies: true do |l|
            l.filter(/(Authorization:) (.+)$/, '\1 [REMOVED]')
          end
        end

        # Enabling error raising
        faraday.use Faraday::Response::RaiseError

        # Enabling json parser
        faraday.response :json,
          content_type: /\bjson$/,
          parser_options: { symbolize_names: true }

        faraday.adapter params.fetch(:adapter) { Faraday.default_adapter }
      end
    end

    ##
    # Отправить СМС сообщение на указанный номер
    #
    # @param phone [String] номер телефона на который будет отправлено сообщение
    # @param text [String] текст сообщения
    #
    # @return [Hash] keys:
    #   - parts [Integer] кол-во частей сообщения
    #   - cost [BigDecimal] стоимость сообщения (за все части)
    #   - balance [BigDecimal] текущий баланс счёта
    #
    # @raise [SMSC::Client::Error]
    #
    def send_message(phone:, text:)
      begin
        resp = @faraday.get "https://smsc.ru/sys/send.php", DEFAULT_PARAMS.merge(
          login: @login,
          psw: @password,
          sender: @sender,
          phones: phone,
          mes: text,
          # Добавить в ответ стоимость сообщения и обновлённый баланс
          cost: 3
        )
      rescue Faraday::Error => e
        raise SMSC::Client::Error, e.message
      end

      body = validate_response(resp.body, SMSC::Client::Validation::SendMessageResponseContract)

      {
        parts: body[:cnt],
        cost: BigDecimal(body[:cost]),
        balance: BigDecimal(body[:balance]),
        response_body: resp.body
      }
    end

    ##
    # Запросить звонок на номер с кодом подтверждения
    #
    # @param phone [String] номер телефона на который будет осуществлён звонок
    #
    # @return [Hash] keys:
    #   - parts [Integer] кол-во частей сообщения
    #   - cost [BigDecimal] стоимость сообщения (за все части)
    #   - balance [BigDecimal] текущий баланс счёта
    #
    # @raise [SMSC::Client::Error]
    #
    def call_to_verify(phone:)
      begin
        resp = @faraday.get "https://smsc.ru/sys/send.php", DEFAULT_PARAMS.merge(
          login: @login,
          psw: @password,
          phones: phone,
          mes: "code",
          call: "1",
          # Добавить в ответ стоимость сообщения и обновлённый баланс
          cost: 3
        )
      rescue Faraday::Error => e
        raise SMSC::Client::Error, e.message
      end

      body = validate_response(resp.body, SMSC::Client::Validation::CallToVerifyResponseContract)

      {
        code: body[:code],
        parts: body[:cnt],
        cost: BigDecimal(body[:cost]),
        balance: BigDecimal(body[:balance]),
        response_body: resp.body
      }
    end

    ##
    # Получить текущий баланс счёта
    #
    # @return [BigDecimal] текущий баланс счёта
    #
    # @raise [SMSC::Client::Error]
    #
    def balance
      begin
        resp = @faraday.get "https://smsc.ru/sys/balance.php", DEFAULT_PARAMS.merge(
          login: @login,
          psw: @password
        )
      rescue Faraday::Error => e
        raise SMSC::Client::Error, e.message
      end

      body = validate_response(resp.body, SMSC::Client::Validation::BalanceResponseContract)

      BigDecimal body[:balance]
    end

    private

    ##
    # Провалидировать тело ответа
    #
    # @param body [Hash] тело ответа
    # @param response_contract [Dry::Validation::Contract] валидационный контракт успешного ответа
    #
    # @raise [SMSC::Client::Error] при наличии ошибок
    #
    # @return [Hash] провалидированное тело успешного ответа
    #
    def validate_response(body, response_contract)
      maybe_error = SMSC::Client::Validation::ErrorResponseContract.new.call(body)

      raise SMSC::Client::Error.new(body: body), maybe_error.to_h[:error] if maybe_error.success?

      maybe_result = response_contract.new.call(body)

      raise SMSC::Client::Error.new(body: body), "Unexpected result" unless maybe_result.success?

      maybe_result.to_h
    end
  end
end
