# frozen_string_literal: true

require "dry-validation"

module SMSC
  class Client
    module Validation
      ##
      # Контракт валидации ответа при отправке звонка с кодом подтверждения
      #
      class CallToVerifyResponseContract < SendMessageResponseContract
        schema do
          required(:code).filled(:string)
        end

        rule(:code) do
          key.failure("must be 6 length digit string") unless value[/^\d{6}$/]
        end
      end
    end
  end
end
