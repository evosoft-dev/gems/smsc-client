# frozen_string_literal: true

require "dry-validation"

module SMSC
  class Client
    module Validation
      ##
      # Контракт валидации ответа при запросе баланса
      #
      class BalanceResponseContract < Dry::Validation::Contract
        schema do
          required(:balance).filled(:string)
        end

        rule(:balance) do
          BigDecimal value
        rescue ArgumentError
          key.failure("must be decimal")
        end
      end
    end
  end
end
