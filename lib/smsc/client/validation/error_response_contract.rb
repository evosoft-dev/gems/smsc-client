# frozen_string_literal: true

require "dry-validation"

module SMSC
  class Client
    module Validation
      ##
      # Контракт валидации ответа с ошибкой
      #
      class ErrorResponseContract < Dry::Validation::Contract
        schema do
          required(:id).value(:integer)
          required(:error).filled(:string)
          required(:error_code).value(:integer)
        end
      end
    end
  end
end
