# frozen_string_literal: true

require "dry-validation"

module SMSC
  class Client
    module Validation
      ##
      # Контракт валидации ответа при отправке СМС
      #
      class SendMessageResponseContract < Dry::Validation::Contract
        schema do
          required(:id).value(:integer)
          required(:cnt) { int? & gt?(0) }
          required(:cost).filled(:string)
          required(:balance).filled(:string)
        end

        rule(:cost) do
          BigDecimal value
        rescue ArgumentError
          key.failure("must be decimal")
        end

        rule(:balance) do
          BigDecimal value
        rescue ArgumentError
          key.failure("must be decimal")
        end
      end
    end
  end
end
