# frozen_string_literal: true

module SMSC
  class Client
    ##
    # Base SMSC client error
    #
    class Error < StandardError
      attr_reader :params

      def initialize(*args, **params)
        super(*args)

        @params = params
      end
    end
  end
end
