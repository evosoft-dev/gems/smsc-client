# frozen_string_literal: true

require "securerandom"

RSpec.describe SMSC::Client do
  let(:login) { SecureRandom.hex }
  let(:password) { SecureRandom.hex }
  let(:sender) { SecureRandom.hex }

  let(:params) do
    { login: login, password: password, sender: sender, logger: Logger.new("/dev/null") }
  end

  subject { described_class.new(**params) }

  it "has a version number" do
    expect(SMSC::Client::VERSION).not_to be nil
  end

  describe "initialization" do
    it "correctly defines instance variables" do
      expect(
        [:@login, :@password, :@sender, :@faraday]
          .map(&subject.method(:instance_variable_get))
      ).to match [login, password, sender, be_a(Faraday::Connection)]
    end

    it "enables faraday logging by default" do
      expect_any_instance_of(Faraday::Connection).to receive(:response).with(:logger, any_args)
      expect_any_instance_of(Faraday::Connection).to receive(:response).with(:json, any_args)

      subject
    end

    context "when `false` is passed for `logging` param" do
      before { params.merge!(logging: false) }

      it "doesn't enable faraday logging" do
        expect_any_instance_of(Faraday::Connection).not_to receive(:response).with(:logger, any_args)

        subject
      end
    end

    context "when custom logger is passed" do
      let(:logger) { Logger.new($stdout) }

      before { params.merge!(logger: logger) }

      it "enables faraday logging with custom logger" do
        expect_any_instance_of(Faraday::Connection).to receive(:response).with(:logger, logger, any_args)
        expect_any_instance_of(Faraday::Connection).to receive(:response).with(:json, any_args)

        subject
      end
    end

    context "when custom adapter is passed" do
      let(:adapter) { :custom_adapter }

      before { params.merge!(adapter: adapter) }

      it "sets custom adapter for faraday" do
        expect_any_instance_of(Faraday::Connection).to receive(:adapter).with(adapter)

        subject
      end
    end
  end

  describe "#send_message" do
    let(:phone) { SecureRandom.hex }
    let(:text) { SecureRandom.hex }

    let(:body) do
      {
        id: 1,
        cnt: 1,
        cost: "5.0",
        balance: "100.0"
      }
    end

    before do
      stub_request(:get, "https://smsc.ru/sys/send.php")
        .with(
          query: described_class::DEFAULT_PARAMS.merge(
            login: login,
            psw: password,
            sender: sender,
            phones: phone,
            mes: text,
            cost: 3
          )
        )
        .to_return(
          body: body.to_json,
          headers: { "Content-Type": "application/json" },
          status: 200
        )
    end

    it "returns correct result" do
      expect(subject.send_message(phone: phone, text: text)).to eq(
        parts: body[:cnt],
        cost: BigDecimal(body[:cost]),
        balance: BigDecimal(body[:balance]),
        response_body: body
      )
    end
  end

  describe "#call_to_verify" do
    let(:phone) { SecureRandom.hex }

    let(:body) do
      {
        id: 1,
        cnt: 1,
        cost: "5.0",
        balance: "100.0",
        code: "123456"
      }
    end

    before do
      stub_request(:get, "https://smsc.ru/sys/send.php")
        .with(
          query: described_class::DEFAULT_PARAMS.merge(
            login: login,
            psw: password,
            phones: phone,
            mes: "code",
            call: "1",
            cost: 3
          )
        )
        .to_return(
          body: body.to_json,
          headers: { "Content-Type": "application/json" },
          status: 200
        )
    end

    it "returns correct result" do
      expect(subject.call_to_verify(phone: phone)).to eq(
        code: body[:code],
        parts: body[:cnt],
        cost: BigDecimal(body[:cost]),
        balance: BigDecimal(body[:balance]),
        response_body: body
      )
    end
  end

  describe "#balance" do
    let(:body) do
      {
        balance: "100.0"
      }
    end

    before do
      stub_request(:get, "https://smsc.ru/sys/balance.php")
        .with(
          query: described_class::DEFAULT_PARAMS.merge(
            login: login,
            psw: password
          )
        )
        .to_return(
          body: body.to_json,
          headers: { "Content-Type": "application/json" },
          status: 200
        )
    end

    it "returns correct result" do
      expect(subject.balance).to eq(BigDecimal(body[:balance]))
    end
  end

  # Private methods

  describe "#validate_response" do
    let(:error_contract) { SMSC::Client::Validation::ErrorResponseContract }
    let(:validation_contract) { SMSC::Client::Validation::BalanceResponseContract }

    let(:error_body) do
      { id: 1, error: SecureRandom.hex, error_code: 1 }
    end

    let(:unexpected_error_body) do
      { example_param: SecureRandom.hex }
    end

    let(:response_body) do
      { balance: "100.0" }
    end

    context "when error contract is valid" do
      it "raises SMSC::Client::Error with correct attributes" do
        expect { subject.send :validate_response, error_body, validation_contract }
          .to raise_error an_instance_of(SMSC::Client::Error).and having_attributes(
            message: error_body[:error],
            params: { body: error_body }
          )
      end
    end

    context "when both error and response contract are invalid" do
      it "raises SMSC::Client::Error with correct attributes" do
        expect { subject.send :validate_response, unexpected_error_body, validation_contract }
          .to raise_error an_instance_of(SMSC::Client::Error).and having_attributes(
            message: "Unexpected result",
            params: { body: unexpected_error_body }
          )
      end
    end

    context "when response contract is valid" do
      it "returns validated body" do
        result = subject.send :validate_response, response_body, validation_contract

        expect(result).to eq(response_body)
      end
    end
  end
end
