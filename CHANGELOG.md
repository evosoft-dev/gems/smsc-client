## [Unreleased]

## [0.1.0] - 2022-01-12

- Initial release
- Add methods `send_message` and `balance`

## [0.1.1] - 2022-01-13

- Changed gem structure

## [0.2.0] - 2022-03-23

- Add method `call_to_verify`

## [0.2.1] - 2022-07-04

- Update Faraday dependency to v2
