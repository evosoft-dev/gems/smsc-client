# frozen_string_literal: true

require_relative "lib/smsc/client/version"

Gem::Specification.new do |spec|
  spec.name          = "smsc-client"
  spec.version       = SMSC::Client::VERSION
  spec.authors       = ["Nikita Tulin"]
  spec.email         = ["ntulin.work@gmail.com"]

  spec.summary       = "SMSC.ru client"
  spec.homepage      = "https://gitlab.com/evosoft-dev/gems/smsc-client"

  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "https://gitlab.com"

  spec.metadata["homepage_uri"]    = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/evosoft-dev/gems/smsc-client"
  spec.metadata["changelog_uri"]   = "https://gitlab.com/evosoft-dev/gems/smsc-client/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "faraday", ">= 2.3"
  spec.add_dependency "dry-validation", "~> 1.7"

  spec.add_development_dependency "rake", "~> 13.0", ">= 13.0.6"
  spec.add_development_dependency "rspec", "~> 3.10"
  spec.add_development_dependency "webmock", "~> 3.14"
  spec.add_development_dependency "rubocop", "~> 1.24", ">= 1.24.1"
  spec.add_development_dependency "rubocop-rake", "~> 0.6.0"
  spec.add_development_dependency "rubocop-rspec", "~> 2.7"
  spec.add_development_dependency "rubocop-performance", "~> 1.13", ">= 1.13.1"
  spec.add_development_dependency "byebug", "~> 11.1", ">= 11.1.3"
end
