# SMSC::Client

SMSC.ru API client

## Installation

Add this line to your application's Gemfile:

```ruby
gem "smsc-client", git: "https://gitlab.com/evosoft-dev/gems/smsc-client.git", tag: "v0.2.1"
```

And then execute:

    $ bundle install

## Usage

```ruby
> client = SMSC::Client.new(login: "login", password: "password", sender: "SMSC.ru")

# Send SMS
> client.send_message(phone: "79000000000", text: "Hello")
=> {:parts=>1, :cost=>5.0, :balance=>0.95e2, :response_body=>{:id=>1, :cnt=>1, :cost=>"5", :balance=>"95.0"}}

# Check balance
> client.balance
=> 0.95e2
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag.
